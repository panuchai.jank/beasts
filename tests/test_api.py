import pytest
import requests

url = "http://panuchaiAnywhere.pythonanywhere.com/"
niffler_id = 1
fairy_id = 4




def test_connection():
    response = requests.get(url)
    assert response.status_code == 200

def test_checkdb():
    response = requests.get(url + '/v1/checkdb')
    response_body = response.json()
    assert response_body["Database connection"][0]["1"] == 1


def test_retrieve_all():
    response = requests.get(url + '/v1/beast')
    assert response.status_code == 200


def test_retrieve_count_all():
    response = requests.get(url + '/v1/beast')
    response_body = response.json()
    assert len(response_body) >= 3


def test_retrieve_all_niffler():
    response = requests.get(url + '/v1/beast')
    response_body = response.json()

    assert response_body[0]["name"] == "niffler"

def test_update_niffler():
    niffler_data = {"name": "niffler"}
    response = requests.put(
        url + '/v1/beast/' + str(niffler_id),
        json = niffler_data)
    response_body = response.json()
    assert response_body["name"] == "niffler"


def test_update_beast_not_exist():
    niffler_data = {"name": "niffler"}
    response = requests.put(
        url + '/v1/beast/99999999',
        json = niffler_data)
    assert response.status_code == 404


def test_update_beast_not_match():
    niffler_data = {"name": "niffler"}
    response = requests.put(
        url + '/v1/beast/' + str(fairy_id),
        json = niffler_data)
    assert response.status_code == 409


def test_create_niffler():
    niffler_data = {"name": "niffler"}
    response = requests.post(
        url + '/v1/beast',
        json = niffler_data)
    assert response.status_code == 409


# Create beasts
def create(beast):
    name = beast.get("name")
    existing_beast = (Beast.query.filter(Beast.name == name).one_or_none())

    # Add new beast
    if existing_beast is None:
        schema = BeastSchema()
        new_beast = schema.load(beast, session=db.session)
        db.session.add(new_beast)
        db.session.commit()

        data = schema.dump(new_beast)
        return data, 201
    # Duplicate beast
    else:
        abort(409, "Beast {name} already exists".format(name=name))
